Does RST support syntax highlighting?

.. code-block:: python

    import sys
    print(sys.version)
    x = 1 + 2 + 3


.. code-block:: python

    >>> import sys
    >>> print(sys.version)
    >>> x = 1 + 2 + 3


.. code:: python

    import sys
    print(sys.version)
    x = 1 + 2 + 3


.. code:: python

    >>> import sys
    >>> print(sys.version)
    >>> x = 1 + 2 + 3
